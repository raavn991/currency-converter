export function rewriteCurrency(amount:number, currencyCode:string) {
	return Intl.NumberFormat(
    "cs-CZ",
		{ 
      style: "currency", 
      currency: currencyCode 
    }
  )
  .format(amount)
}
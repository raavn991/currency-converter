import styled from 'styled-components'
import { QueryClient, QueryClientProvider } from 'react-query'
import ConversionForm from './Components/ConversionForm'
import ExchangeRatesTable from './Components/ExchangeRatesTable'

// blue: #4D61FC
// black: #303030
// grey: #D8DDE6

const AppContainer = styled.div`
  font-family: 'Lato', sans-serif;
  background: #ffffff;
  color: #303030;
`

const PageTitle = styled.h1`
  font-size: 3em;
  width: 100%;
  text-align: center;
  margin: 100px auto;
`

const queryClient = new QueryClient()

export const fetchExchangeRates = async () => {
  // const apiUrl = 'https://' +
  // 'www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu' +
  // '/denni_kurz.txt'
  const apiUrl = 'https://cnbproxy.fso.solutions/' +
  'www.cnb.cz/cs/financni_trhy/devizovy_trh/' + 
  'kurzy_devizoveho_trhu/denni_kurz.txt'

  const res = await fetch(apiUrl, {
    method: 'GET',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/txt',
    }
  })
  return res.text()
}

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <AppContainer>
        <PageTitle>Currency converter</PageTitle>
        <ConversionForm />
        <ExchangeRatesTable />
      </AppContainer>
    </QueryClientProvider>
  );
}

export default App

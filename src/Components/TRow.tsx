import styled from 'styled-components'
import { rewriteCurrency } from '../Utils/rewriteCurrency'

export interface TRowProps {
  currencyData: {
    countryName: string,
    currencyName: string,
    currencyCode: string,
    czkBatch: number,
    exchangeValue: number,
  },
  key: string,
}

export const TabRow = styled.tr`
  height: 30px;
  border-spacing: 0;
  border-collapse: collapse;
  &:hover {
    background: #eeeeee;
  }
  & td {
    padding: 0;
  }
`
 
const TRow: React.FunctionComponent<TRowProps> = ({currencyData}:TRowProps) => {
  return ( 
    <TabRow>
      <td>{currencyData.countryName}</td>
      <td>{currencyData.currencyName}</td>
      <td>
        {rewriteCurrency(currencyData.czkBatch, currencyData.currencyCode)}
        {' = '}
        {rewriteCurrency(currencyData.exchangeValue, 'CZK')}
      </td>
    </TabRow>  
   )
}
 
export default TRow
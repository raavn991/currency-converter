import React, { useState } from 'react'
import styled from 'styled-components'
import IconArrow from './IconArrow'
import { useQuery } from 'react-query'
import { fetchExchangeRates } from '../App'
import { rewriteCurrency } from '../Utils/rewriteCurrency'

const Input = styled.input`
  display: inline-block;
  height: 44px;
  width: 70%;
  padding: 0 20px;
  border: 2px solid #D8DDE6;
  border-radius: 4px;
  background-color: rgba(246,246,246,0.3);
  font-size: 14px;
  margin-right: 15px;
`

const CzkInput = styled(Input)`
  transition: border-color .2s ease,background-color .2s ease;
`

const SubmitButton = styled.button`
  display: flex;
  background-color: #4D61FC;
  color: #FFFFFF;
  border: 0;
  border-radius: 4px;
  box-shadow:
    0 2px 4px 0 rgba(136,144,195,0.2),
    0 5px 15px 0 rgba(37,44,97,0.15);
  font-weight: 700;
  padding: 16px 40px;
`

const FormBody = styled.form`
  width: 70vw;
  display: flex;
  justify-content: space-between;
  margin: 0 auto;
`

const CurrencySelect = styled.select`
  display: inline-block;
  height: 48px;
  width: 50%;
  padding: 0 20px;
  border: 2px solid #D8DDE6;
  border-radius: 4px;
  background-color: rgba(246,246,246,0.3);
  font-size: 14px;
  margin-right: 15px;
  background:
    url(https://cdn1.iconfinder.com/data/icons/cc_mono_icon_set/blacks/16x16/br_down.png)
    no-repeat right;
  -webkit-appearance: none;
  background-size: 12px;
  background-position-x: calc(100% - 15px);
`

const ButtonText = styled.span`
  margin-right: 10px;
`

const ConversionResult = styled.p`
  width: 70vw;
  margin: 60px auto;
  font-weight: 100;
  font-size: 2em;
`

const Option = styled.option`
  text-transform: capitalize;
`

const Error = styled.span`
  color: #cc0000;
  font-weight: 700;
`

const CText = styled.div`
  width: 155px;
  height: 31px;
  padding-top: 13px;
`

export interface IConversion {
  currencyCode: string,
  czkBatch: number,
  exchangeValue: number,
}

let lines: Array<string> = []
let conversion: Array<IConversion> = []

const ConversionForm: 
  React.FC = () => {
    const {data, status} = useQuery('exchangeRates', fetchExchangeRates)
    const [result, setResult] = useState({
      output: 0,
      currency: '',
      czk: 0
    })
    const [formSubmitState, setFormSubmitState] = useState(false)
    
    if (data && status) {
      lines = data?.split('\n')
      conversion = []
      lines.slice(2, -1).forEach(item => {
        const itemSlice = item.split('|')
        conversion.push(
          {
            currencyCode: itemSlice[3],
            czkBatch: parseFloat(itemSlice[2]),
            exchangeValue: parseFloat(itemSlice[4].replace(',', '.'))
          }
        )
      })
    }

    const handleSubmit = (e: React.ChangeEvent<HTMLFormElement>) => {
      e.preventDefault()
      const currencySelect = e.target.currencySelect.value
      let czkInput = e.target.czkInput.value
      if (conversion)
        {
          const currencyMatch = 
            conversion.find(item => item.currencyCode === currencySelect)
          let conversionBatch = 1
          let conversionRate = 1
          currencyMatch 
            ? conversionBatch = currencyMatch.czkBatch
            : conversionBatch = 1
          currencyMatch
            ? conversionRate = currencyMatch.exchangeValue
            : conversionRate = 1
          isNaN(czkInput) && (czkInput = czkInput.replace(',', '.'))
          setResult(
            {
              output: (czkInput/conversionRate)*conversionBatch,
              currency: currencySelect,
              czk: czkInput
            }
          )
          setFormSubmitState(true)
        }
    }

    return (
      <>
        <FormBody onSubmit={handleSubmit}>
          <CzkInput
            placeholder = 'Amount in CZK'
            required
            name = 'czkInput'
          />
          <CText>CZK to </CText>
          <CurrencySelect
            required
            name = 'currencySelect'
            >
              <Option value=''>select a currency</Option>
            {
            conversion.map(item => 
              <Option key={item.currencyCode} value={item.currencyCode}>
                {item.currencyCode}
              </Option>
              )
            }
          </CurrencySelect>
          <SubmitButton>
            <ButtonText>Convert</ButtonText>
            <IconArrow />
          </SubmitButton>
        </FormBody>

        <ConversionResult>
          {(formSubmitState === true && result.czk > 0) &&
            'You can get ' + 
            rewriteCurrency(result.output, result.currency) +
            ' for ' +
            rewriteCurrency(result.czk, 'CZK')
          }

          {(formSubmitState === true && result.czk < 0) && 
            <Error>Conversion cannot be for less than 0 Kč</Error>
          }

          {(formSubmitState === true && isNaN(result.czk)) && 
            <Error>Please use a number for conversion</Error>
          }
        </ConversionResult>
      </>
    )
  }
 
export default ConversionForm
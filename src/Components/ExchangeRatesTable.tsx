import React from 'react'
import styled from 'styled-components'
import { useQuery } from 'react-query'
import { fetchExchangeRates } from '../App'
import Loader from './Loader'
import TRow from './TRow'

const Table = styled.table`
  box-shadow:
    0 2px 4px 0 rgba(136,144,195,0.2),
    0 5px 15px 0 rgba(37,44,97,0.15);
  width: 70vw;
  height: 100px;
  border-radius: 10px;
  border-spacing: 0;
  margin: 30px auto;
  padding: 30px;
`

const THeader = styled.thead`
  font-weight: 900;
  font-size: 1.2em;
  line-height: 70px;
`

const TBody = styled.tbody`

`

const ErrorMessage = styled.p`
  color: #CC0000;
`

export interface Currency {
  countryName: string,
  currencyName: string,
  currencyCode: string,
  czkBatch: number,
  exchangeValue: number,
}

let lines:Array<string> = []
let currency: Array<Currency> = []
let rateDate:string = 'unknown'
 
const ExchangeRatesTable:
  React.FC = () => {
    const {data, status} = useQuery('exchangeRates', fetchExchangeRates)
    
    if (data && status) {
      lines = data?.split('\n')
      rateDate = lines[0]
      currency = []
      lines.slice(2, -1).forEach(item => {
        const itemSlice = item.split('|')
        currency.push(
          {
            countryName: itemSlice[0],
            currencyName: itemSlice[1],
            currencyCode: itemSlice[3],
            czkBatch: parseFloat(itemSlice[2]),
            exchangeValue: parseFloat(itemSlice[4].replace(',', '.'))
          }
        )
      })
    }

    return ( 
      <Table>
        <THeader>
          <tr>
            <td colSpan={4}>

              { status === 'loading' && (
                'Loading rates, please wait...'
              ) }
              
              { (status === 'error') && (
                <ErrorMessage>
                  There was an error while fetching data
                </ErrorMessage>
              ) }

              { status === 'success' && `Exchange rates version: ${rateDate}` }

            </td>
          </tr>
        </THeader>
        
        <TBody>
          { status === 'loading' && (
            <tr>
              <td colSpan={4}>
                <Loader />
              </td>
            </tr>
          ) }

          { status === 'success' &&
            currency.map(item => 
                <TRow
                key = {item.currencyCode}
                currencyData = {item}
               />
            )
          }
        </TBody>
      </Table>
    )
}
 
export default ExchangeRatesTable
